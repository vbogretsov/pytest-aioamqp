"""PyTest plugin to mock aioamqp calls.

To mock aioamqp add dependency to the fixture aioamqpmock in the fixture where
the method aioamqp.connect is called. Pass option --mockamqp to enable the
aioamqpmock fixture.
"""
from unittest import mock

import attrdict
import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--mockamqp",
        action="store_true",
        dest="mockamqp",
        default=False,
        help="specify whether AMQP mock should"
        "be used instead of a real serivce.")


class AMQPMockError(Exception):
    """Occurs when publishing to not existing queue.

    The library aioamqp does not raise any error in that case, but it's better
    to discover such errors as soon as possible.
    """


class AsyncMock(mock.MagicMock):
    """MagicMock for async calls.
    """

    async def __call__(self, *args, **kwargs):
        return super().__call__(self, *args, **kwargs)


class AMQPChannelMock(AsyncMock):
    """Mock for aioamqp.Channel.

    Mocked methods:
        basic_consume
        basic_publish
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queues = {}

    async def basic_consume(self, callback, queue_name="", **kwargs):
        self.queues[queue_name] = callback

    async def basic_publish(self,
                            payload,
                            exchange_name,
                            routing_key,
                            properties=None,
                            **kwargs):
        if routing_key not in self.queues:
            raise AMQPMockError("queue [%s] does not exist" % routing_key)
        properties = properties or {}
        await self.queues[routing_key](self, payload, mock.MagicMock(),
                                       attrdict.AttrDict(**properties))


async def connect_mock(**kwargs):
    """Mock for the function aioamqp.connect.
    """

    async def channel():
        return AMQPChannelMock()

    return mock.MagicMock(), AsyncMock(channel=channel)


@pytest.fixture
def aioamqpmock(request):
    """Use this fixture to mock AMQP.

    Pass option --mockamqp to enable this fixture.
    """
    if request.config.option.mockamqp:
        aioamqp_mock = mock.patch("aioamqp.connect", new=connect_mock)
        aioamqp_mock.start()
        request.addfinalizer(aioamqp_mock.stop)
