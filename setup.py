# -*- coding:utf-8 -*-
"""A setuptools based setup module for pytest-aioamqp.
"""

import setuptools

setuptools.setup(
    name="pytest-aioamqp",
    version="0.1.0",
    description="Provides fixture that mocks aioamqp objects.",
    url="https://gitlab.com/vbogretsov/pytest-aioamqp",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    packages=setuptools.find_packages(exclude=[".env", "tests"]),
    setup_requires=["pytest-runner"],
    tests_require=["pytest", "pytest-asyncio", "aioamqp"],
    entry_points={"pytest11": [
        "aioamqp = pytest_aioamqp",
    ]},
    install_requires=["pytest", "attrdict"])
