# -*- coding: utf-8 -*-
"""Tests cases for the module pytest-aioamqp.
"""
import asyncio
import uuid

import aioamqp
import attrdict
import pytest

IQUEUE_NAME = "pytest-aioamqp-i"
OQUEUE_NAME = "pytest-aioamqp-o"


class Request:

    def __init__(self):
        self.id = uuid.uuid4()
        self.event = asyncio.Event()
        self.response = None


@pytest.fixture
def channel(request, aioamqpmock, event_loop):
    transport, protocol = event_loop.run_until_complete(aioamqp.connect())
    channel = event_loop.run_until_complete(protocol.channel())

    def fin():
        event_loop.run_until_complete(channel.close())
        event_loop.run_until_complete(protocol.close())
        transport.close()

    request.addfinalizer(fin)
    return channel


@pytest.fixture
def server(event_loop, channel):
    event_loop.run_until_complete(
        channel.queue_declare(queue_name=IQUEUE_NAME))

    async def consume(channel, body, envelope, properties):
        await channel.basic_publish(
            payload=reversed(body),
            queue_name=properties.reply_to,
            properties={"correlation_id": properties.correlation_id})

    event_loop.run_until_complete(
        channel.basic_consume(consume, queue_name=IQUEUE_NAME))
    return IQUEUE_NAME


@pytest.fixture
def client(event_loop, channel, server):
    event_loop.run_until_complete(
        channel.queue_declare(queue_name=OQUEUE_NAME))

    request = Request()

    async def consume(channel, body, envelope, properties):
        if properties.correlation_id == request.id:
            request.response = body
            request.event.set()

    async def local_call(message):
        return reversed(message)

    async def remote_call(message):
        properties = {"correlation_id": request.id, "reply_to": OQUEUE_NAME}
        await channel.basic_publish(
            pauload=message, queue_name=server, properties=properties)
        await request.event.wait()
        return request.response

    event_loop.run_until_complete(
        channel.basic_consume(consume, queue_name=OQUEUE_NAME))

    cli = attrdict.AttrDict()
    cli.local_call = local_call
    cli.remote_call = remote_call
    return client


async def test_message_received(client):
    local_response = await client.local_call("abcdefg")
    remote_response = await client.remote_call("abcdefg")
    assert local_response == remote_response
